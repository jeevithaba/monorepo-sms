import { Link } from 'react-router-dom';
import classes from './MainNavigation.module.css';

const MainNavigation = () => {
  return (
    <header className={classes.header}>
       <Link to='/'>
        <div className={classes.logo}>Student Management System</div>
      </Link>
      <nav>
        <ul>
            <li>
              <Link to='/login'>Login</Link>
            </li>
            <li>
              <Link to='/home'>Home</Link>
            </li>
            <li>
              <Link to='/profile'>Profile</Link>
            </li>
         </ul>
      </nav>
    </header>
  );
};

export default MainNavigation;