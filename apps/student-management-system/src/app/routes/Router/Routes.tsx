import { VFC } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from 'react-router-dom';
import Home from '../../pages/Home/Home';
import Login from '../../pages/Login/Login';
import Profile from '../../pages/Profile/Profile';
import Layout from '../Layout/Layout';

const Routes: VFC = () => {
  return (
    <Router>
        <Layout>
      <Switch>
      <Route path='/' exact>
        <Home />
      </Route>
     
      <Route path='/login' >
        <Login />
      </Route>

      <Route path='/home'>
        <Home/>
        </Route>
      
      <Route path='/profile'> 
      <Profile />
     </Route>

      <Route path='*'>
        <Redirect to='/'/>
        </Route>
      </Switch>
      </Layout>
    </Router>
  );
};

export default Routes;
