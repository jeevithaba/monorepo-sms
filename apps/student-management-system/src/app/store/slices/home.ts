import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import axios, { AxiosResponse } from 'axios';
import api from '../../utils/api';


export interface TodoState {
  loading: boolean;
}

const initialState: TodoState = {
  loading: false,
};



export const HomeSlice = createSlice({
  name: 'home',
  initialState,
  reducers:{}

});

export const HomeAction = HomeSlice.actions;

export default HomeSlice.reducer;
