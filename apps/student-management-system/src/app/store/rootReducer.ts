import { combineReducers } from "@reduxjs/toolkit";
import homeReducer from "./slices/home";

const rootReducer = combineReducers({
  home: homeReducer,
});

export default rootReducer;
