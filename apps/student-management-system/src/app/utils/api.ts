import axios, { AxiosInstance } from "axios";

const api: AxiosInstance = axios.create({
  baseURL: process.env.NX_API_BASE_URL,
  headers: {
    "Content-Type": "application/json",
  },
});

export default api;
