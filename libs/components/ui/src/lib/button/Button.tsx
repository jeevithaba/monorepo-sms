import Button from '@mui/material/Button';
import { VFC } from 'react';
export interface PrimaryButtonProps {
  title: string;
  onClick: () => void;
}

const PrimaryButton: VFC<PrimaryButtonProps> = (props) => {
  const { title, onClick } = props;
  return (
    <Button onClick={onClick} variant="contained">
      {title}
    </Button>
  );
};

export default PrimaryButton;