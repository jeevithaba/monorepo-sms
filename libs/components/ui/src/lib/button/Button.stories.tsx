import { Story, Meta } from '@storybook/react';
import PrimaryButton, { PrimaryButtonProps } from './Button';

export default {
  component: PrimaryButton,
  title: 'Button',
  argTypes: { onClick: { action: 'clicked' } },
} as Meta;

const Template: Story<PrimaryButtonProps> = (args) => (
  <PrimaryButton {...args} />
);

export const Primary = Template.bind({});
Primary.args = {
  title: 'Primary',
  // onClick: () => {
  //   console.log('clicked');
  // },
};
